# CARP JWT Plugin

The plugin override request to web servers when the request start and add a session cookie to request when the request end

## 🔨 Build

 tinygo is required

```bash
tinygo build -o jwt ./cmd/main/main.go
```

## ⭐ How to use

The plugin expose the next events:

- `configure`
- `request_start`
- `request_end`

## 🐞 Testing

In the root of the poject exec:

```bash
tinygo build -o plugin.wasm -tags="purego noasm" -scheduler=none --no-debug -target=wasi cmd/main/main.go
```

## 🐞 Testing

Build the plugin.wasm and later exec in the root of the poject:

```bash
go test ./.../
```
