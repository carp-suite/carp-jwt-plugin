package main

import (
	"gitlab.com/carp-suite/plugins/carp-jwt-plugin/internal/core"
	"gitlab.com/carp-suite/plugins/carpdriver"
)

func main() {
	carpdriver.Init(&core.JwtPlugin{})
}
