module gitlab.com/carp-suite/plugins/carp-jwt-plugin

go 1.19

require (
	github.com/buger/jsonparser v1.1.1
	github.com/cucumber/godog v0.12.6
	github.com/cucumber/messages-go/v16 v16.0.1
	github.com/google/flatbuffers v23.5.26+incompatible
	github.com/google/uuid v1.3.1
	github.com/rdumont/assistdog v0.0.0-20201106100018-168b06230d14
	github.com/stretchr/testify v1.8.4
	github.com/wapc/wapc-go v0.6.0
	gitlab.com/carp-suite/plugins/carpdriver v0.0.0-20231017211809-56182c6717ae
	gitlab.com/carp-suite/plugins/tinyjwt v1.13.0
)

require (
	github.com/Workiva/go-datastructures v1.0.53 // indirect
	github.com/cucumber/gherkin-go/v19 v19.0.3 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gofrs/uuid v4.2.0+incompatible // indirect
	github.com/hashicorp/go-immutable-radix v1.3.1 // indirect
	github.com/hashicorp/go-memdb v1.3.2 // indirect
	github.com/hashicorp/golang-lru v0.5.4 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/tetratelabs/wazero v1.0.0 // indirect
	github.com/wapc/wapc-guest-tinygo v0.3.3 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
