Feature: JWT Plugin
  The plugin override request to web servers when the request start
  and add a session cookie to request when the request end

  Scenario: Set plugin configuration
    Given a configuration:
		"""
		{
			"cookieName": "JWT",
			"secretKey": "hello"
		}
		"""
    When configure the plugin
    Then the plugin should have the next configuration:
      | CookieName | JWT   |
      | SecretKey  | hello |

  Scenario: Override plugin request
    Given the next configuration:
		"""
		{
			"cookieName": "JWT",
			"secretKey": "hello"
		}
		"""
    And the request have id e38a9003-bc2d-4ba3-ac5d-d6a8c74de162
    And path /
		And headers 
			| Content-Length | 11        |
			| Host           | localhost |
		And cookies
			| JWT | eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.Brp7tDCUj3wlZtF6a15KW0A7wLJbnDLOFky03GY9vSdEYo-RlwFCIqpzFV0hHsH5_A7pA28yrRFPqyTSsumZfQ |
		And body
		"""
			hello world
		"""
    When the request start
    Then the request should hold the following headers:
			| Content-Length | 11        |
			| Host           | localhost |
			| Authorization  | Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.Brp7tDCUj3wlZtF6a15KW0A7wLJbnDLOFky03GY9vSdEYo-RlwFCIqpzFV0hHsH5_A7pA28yrRFPqyTSsumZfQ |

  Scenario: Fail override plugin request because cookie no exists
    Given the next configuration:
		"""
		{
			"cookieName": "JWT",
			"secretKey": "hello"
		}
		"""
    And the request have id e38a9003-bc2d-4ba3-ac5d-d6a8c74de162
    And path /
		And headers 
			| Content-Length | 0         |
			| Host           | localhost |
    When the request start
    Then I should get a response e38a9003-bc2d-4ba3-ac5d-d6a8c74de162
    And the response should hold the following headers:
      | Content-Type   | text/plain |
			| Content-Length | 20         |
    And hold path /
    And hold no cookies
    And hold status 403
    And hold body
    """
    JWT cookie not found
    """

  Scenario: Fail override plugin request because error invalid signature
    Given the next configuration:
		"""
		{
			"cookieName": "JWT",
			"secretKey": "hello"
		}
		"""
    And the request have id e38a9003-bc2d-4ba3-ac5d-d6a8c74de162
    And path /
		And headers 
			| Content-Length | 0         |
			| Host           | localhost |
		And cookies
			| JWT | eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWUsImlhdCI6MTUxNjIzOTAyMn0.tyh-VfuzIxCyGYDlkBA7DfyjrqmSHu6pQ2hoZuFqUSLPNY2N0mpHb3nk5K17HWP_3cYHBw7AhHale5wky6-sVA |
    When the request start
    Then I should get a response e38a9003-bc2d-4ba3-ac5d-d6a8c74de162
    And the response should hold the following headers:
      | Content-Type   | text/plain |
			| Content-Length | 33         |
    And hold path /
    And hold no cookies
    And hold status 403
    And hold body
    """
    jwt: algorithm "ES256" not in use
    """

  Scenario: Fail override plugin request because token is expired
    Given the next configuration:
		"""
		{
			"cookieName": "JWT",
			"secretKey": "AllYourBase"
		}
		"""
    And the request have id e38a9003-bc2d-4ba3-ac5d-d6a8c74de162
    And path /
		And headers 
			| Content-Length | 0         |
			| Host           | localhost |
		And cookies
			| JWT | eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmb28iOiJiYXIiLCJleHAiOjE1MDAwLCJpc3MiOiJ0ZXN0In0.HE7fK0xOQwFEr4WDgRWj4teRPZ6i3GLwD5YCm6Pwu_c |
    When the request start
    Then I should get a response e38a9003-bc2d-4ba3-ac5d-d6a8c74de162
    And the response should hold the following headers:
      | Content-Type   | text/plain |
			| Content-Length | 35         |
    And hold path /
    And hold no cookies
    And hold status 401
    And hold body
    """
    jwt: expiration time ["exp"] passed
    """

  Scenario: Fail override plugin request because error parsing jwt
    Given the next configuration:
		"""
		{
			"cookieName": "JWT",
			"secretKey": "hello"
		}
		"""
    And the request have id e38a9003-bc2d-4ba3-ac5d-d6a8c74de162
    And path /
		And headers 
			| Content-Length | 0         |
			| Host           | localhost |
		And cookies
			| JWT | tokenMalformed |
    When the request start
    Then I should get a response e38a9003-bc2d-4ba3-ac5d-d6a8c74de162
    And the response should hold the following headers:
      | Content-Type   | text/plain |
			| Content-Length | 35         |
    And hold path /
    And hold no cookies
    And hold status 403
    And hold body
    """
    jwt: one part only—payload absent
    """

  Scenario: Override plugin response
    Given the next configuration:
		"""
		{
			"cookieName": "JWT",
			"path": "/a",
			"status": 200
		}
		"""
    And the response have id e38a9003-bc2d-4ba3-ac5d-d6a8c74de162
		And headers 
      | Content-Type   | text/plain |
			| Content-Length | 198        |
    And path /app
    And status 200
		And body
    """
    eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.Brp7tDCUj3wlZtF6a15KW0A7wLJbnDLOFky03GY9vSdEYo-RlwFCIqpzFV0hHsH5_A7pA28yrRFPqyTSsumZfQ
    """
    When the request end
    Then I should get a response e38a9003-bc2d-4ba3-ac5d-d6a8c74de162
    And the response should hold the following headers:
      | Content-Type   | text/plain |
			| Content-Length | 0          |
    And hold path /app
		And cookies
			| JWT | eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.Brp7tDCUj3wlZtF6a15KW0A7wLJbnDLOFky03GY9vSdEYo-RlwFCIqpzFV0hHsH5_A7pA28yrRFPqyTSsumZfQ |
    And hold status 200
    And hold body
    """
    """

  Scenario: Fail override plugin response because the status is not equal to the configuration
    Given the next configuration:
		"""
		{
			"path": "/app",
			"status": 404
		}
		"""
    And the response have id e38a9003-bc2d-4ba3-ac5d-d6a8c74de162
    And path /app
    And status 200
    When the request end
    Then I should not get a response 


  Scenario: Fail override plugin response because the path is not similar to the configuration
    Given the next configuration:
		"""
		{
			"path": "/opp",
			"status": 200
		}
		"""
    And the response have id e38a9003-bc2d-4ba3-ac5d-d6a8c74de162
    And path /app
    And status 200
    When the request end
    Then I should not get a response 
