package core

import (
	"fmt"
	"strings"
	"time"

	"github.com/buger/jsonparser"
	"gitlab.com/carp-suite/plugins/tinyjwt"

	"gitlab.com/carp-suite/plugins/carpdriver"
)

type JwtPlugin struct {
	CookieName string
	SecretKey  string
	Path       string
	Status     int64
	Chunks     int64
}

func (p *JwtPlugin) Configure(ctx []byte) error {
	p.CookieName, _ = jsonparser.GetString(ctx, "cookieName")
	p.SecretKey, _ = jsonparser.GetString(ctx, "secretKey")
	p.Path, _ = jsonparser.GetString(ctx, "path")
	p.Status, _ = jsonparser.GetInt(ctx, "status")
	p.Chunks, _ = jsonparser.GetInt(ctx, "chunks")
	if p.Chunks == 0 {
		p.Chunks = 512
	}

	return nil
}

func (p *JwtPlugin) RequestStart(req *carpdriver.Request) (*carpdriver.Request, *carpdriver.Response, error) {
	index := -1
	for i, c := range req.Cookies {
		if c.Name == p.CookieName {
			index = i
			break
		}
	}

	if index == -1 {
		body := []byte("JWT cookie not found")
		headers := make(map[string]string)
		headers["Content-Type"] = "text/plain"
		headers["Content-Length"] = fmt.Sprintf("%d", len(body))

		res := &carpdriver.Response{
			ID:      req.ID,
			Path:    req.Path,
			Headers: headers,
			Cookies: make([]*carpdriver.Cookie, 0),
			Status:  403,
			Body:    body,
		}

		return nil, res, nil
	}

	token := []byte(req.Cookies[index].Value)

	claims, err := tinyjwt.HMACCheck(token, []byte(p.SecretKey))
	if err != nil {
		body := []byte(err.Error())
		headers := make(map[string]string)
		headers["Content-Type"] = "text/plain"
		headers["Content-Length"] = fmt.Sprintf("%d", len(body))

		res := &carpdriver.Response{
			ID:      req.ID,
			Path:    req.Path,
			Headers: headers,
			Cookies: make([]*carpdriver.Cookie, 0),
			Status:  403,
			Body:    body,
		}

		return nil, res, nil
	}

	err = claims.AcceptTemporal(time.Now(), time.Second)
	if err != nil {
		body := []byte(err.Error())
		headers := make(map[string]string)
		headers["Content-Type"] = "text/plain"
		headers["Content-Length"] = fmt.Sprintf("%d", len(body))

		res := &carpdriver.Response{
			ID:      req.ID,
			Path:    req.Path,
			Headers: headers,
			Cookies: make([]*carpdriver.Cookie, 0),
			Status:  401,
			Body:    body,
		}

		return nil, res, nil
	}

	req.Headers["Authorization"] = fmt.Sprintf("Bearer %s", req.Cookies[index].Value)
	return req, nil, nil
}

func (p *JwtPlugin) RequestEnd(res *carpdriver.Response) (*carpdriver.Response, error) {
	if !strings.HasPrefix(res.Path, p.Path) || res.Status != int(p.Status) {
		return nil, nil
	}

	if res.Cookies == nil {
		res.Cookies = make([]*carpdriver.Cookie, 0)
	}

	index := -1
	for i, c := range res.Cookies {
		if c.Name == p.CookieName {
			index = i
			break
		}
	}

	if index != -1 {
		res.Cookies[index].Value = string(res.Body)
	} else {
		res.Cookies = append(res.Cookies, &carpdriver.Cookie{
			Path:  res.Path,
			Name:  p.CookieName,
			Value: string(res.Body),
		})
	}

	res.Body = []byte("")

	for k := range res.Headers {

		if strings.ToLower(k) == "content-length" {
			res.Headers[k] = "0"
		}

		if strings.ToLower(k) == "content-type" {
			res.Headers[k] = "text/plain"
		}
	}

	return res, nil
}

func (p *JwtPlugin) CacheStart(c *carpdriver.Cache) (bool, error) {
	return false, nil
}

func (p *JwtPlugin) CacheEnd(c *carpdriver.Cache) error {
	return nil
}
