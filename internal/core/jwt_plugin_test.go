package core_test

import (
	"context"
	"strings"
	"testing"

	"github.com/cucumber/godog"
	"github.com/google/uuid"
	"github.com/rdumont/assistdog"
	"github.com/stretchr/testify/assert"
	"gitlab.com/carp-suite/plugins/carp-jwt-plugin/internal/core"
	"gitlab.com/carp-suite/plugins/carp-jwt-plugin/internal/helpers"
	"gitlab.com/carp-suite/plugins/carpdriver"
)

type jwtPluginTest struct {
	plugin        *core.JwtPlugin
	result        interface{}
	configuration *godog.DocString
	request       *carpdriver.Request
	response      *carpdriver.Response
}

func TestJwtPlugin(t *testing.T) {
	suite := godog.TestSuite{
		ScenarioInitializer: InitializeScenario,
		Options: &godog.Options{
			Format:   "pretty",
			Paths:    []string{"./jwt_plugin.feature"},
			TestingT: t,
		},
	}

	if suite.Run() != 0 {
		t.Fatal("non-zero status returned, failed to run plugin tests")
	}
}

func InitializeScenario(sc *godog.ScenarioContext) {
	jpt := &jwtPluginTest{}

	sc.Before(func(ctx context.Context, sc *godog.Scenario) (context.Context, error) {
		err := jpt.setup(sc)
		return ctx, err
	})

	sc.Step(`^a configuration:$`, jpt.setConfiguration)
	sc.Step(`^configure the plugin$`, jpt.configureTest)
	sc.Step(`^I should get a parse json error$`, jpt.shouldBeAError)
	sc.Step(`^the plugin should have the next configuration:$`, jpt.shouldHaveTheNextConfiguration)
	sc.Step(`^the next configuration:$`, jpt.configure)
	sc.Step(`^the (\w+) have id (.+)$`, jpt.setId)
	sc.Step(`^path (.+)$`, jpt.setPath)
	sc.Step(`^headers$`, jpt.setHeaders)
	sc.Step(`^cookies$`, jpt.setCookies)
	sc.Step(`^body$`, jpt.setBody)
	sc.Step(`^status (\d+)$`, jpt.setStatus)
	sc.Step(`^the request start$`, jpt.requestStart)
	sc.Step(`^the request end$`, jpt.requestEnd)
	sc.Step(`^the (\w+) should hold the following headers:$`, jpt.shouldHoldHeaders)
	sc.Step(`^I should get a response (.+)$`, jpt.responseShouldHoldId)
	sc.Step(`^I should not get a response$`, jpt.responseShouldNoExists)
	sc.Step(`^hold path (.+)$`, jpt.responseShouldHoldPath)
	sc.Step(`^hold no cookies$`, jpt.responseShouldHoldNoCookies)
	sc.Step(`^hold status (\d+)$`, jpt.responseShouldHoldStatus)
	sc.Step(`^hold body$`, jpt.responseShouldHoldBody)
	sc.Step(`^body must start with$`, jpt.responseShouldHoldBodyStart)
}

func (jpt *jwtPluginTest) setup(*godog.Scenario) error {
	jpt.plugin = new(core.JwtPlugin)
	jpt.request = new(carpdriver.Request)
	jpt.request.Headers = make(map[string]string)
	jpt.request.Cookies = make([]*carpdriver.Cookie, 0, 0)
	jpt.response = new(carpdriver.Response)
	jpt.response.Headers = make(map[string]string)
	jpt.response.Cookies = make([]*carpdriver.Cookie, 0, 0)
	return nil
}

func (jpt *jwtPluginTest) configure(data *godog.DocString) error {
	err := jpt.plugin.Configure([]byte(data.Content))
	if err != nil {
		return helpers.AssertError(assert.NoError, err, err.Error())
	}
	return nil
}

func (jpt *jwtPluginTest) setConfiguration(data *godog.DocString) error {
	jpt.configuration = data
	return nil
}

func (jpt *jwtPluginTest) configureTest() error {
	jpt.result = jpt.configure(jpt.configuration)
	return nil
}

func (jpt *jwtPluginTest) shouldBeAError() error {
	err := jpt.result.(error)
	if err != nil {
		return helpers.AssertError(assert.Error, err, err.Error())
	}
	return nil
}

func (jpt *jwtPluginTest) shouldHaveTheNextConfiguration(configuration *godog.Table) error {
	assist := assistdog.NewDefault()
	c, err := assist.ParseMap(configuration)
	if err != nil {
		return helpers.AssertError(assert.NoError, err, err.Error())
	}

	err = helpers.AssertExpectedActual(
		assert.Equal, c["CookieName"], jpt.plugin.CookieName,
		"Expected %s, but there is %s", c["CookieName"], jpt.plugin.CookieName,
	)
	if err != nil {
		return err
	}

	return helpers.AssertExpectedActual(
		assert.Equal, c["SecretKey"], jpt.plugin.SecretKey,
		"Expected %s, but there is %s", c["SecretKey"], jpt.plugin.SecretKey,
	)
}

func (jpt *jwtPluginTest) setId(r string, id string) error {
	uid, err := uuid.Parse(id)
	if err != nil {
		return helpers.AssertError(assert.NoError, err, err.Error())
	}

	if r == "request" {
		jpt.request.ID = uid
	} else {
		jpt.response.ID = uid
	}

	return nil
}

func (jpt *jwtPluginTest) setPath(path string) error {
	if jpt.request.ID != uuid.Nil {
		jpt.request.Path = path
	} else {
		jpt.response.Path = path
	}

	return nil
}

func (jpt *jwtPluginTest) setHeaders(headers *godog.Table) error {
	assist := assistdog.NewDefault()
	result, err := assist.ParseMap(headers)
	if err != nil {
		return helpers.AssertError(assert.NoError, err, err.Error())
	}

	if jpt.request.ID != uuid.Nil {
		jpt.request.Headers = result
	} else {
		jpt.response.Headers = result
	}

	return nil
}

func (jpt *jwtPluginTest) setCookies(cookies *godog.Table) error {
	assist := assistdog.NewDefault()
	result, err := assist.ParseMap(cookies)
	if err != nil {
		return helpers.AssertError(assert.NoError, err, err.Error())
	}

	if jpt.request.ID != uuid.Nil {
		for k, v := range result {
			jpt.request.Cookies = append(jpt.request.Cookies, &carpdriver.Cookie{
				Path:  jpt.request.Path,
				Name:  k,
				Value: v,
			})
		}
	} else {
		for k, v := range result {
			jpt.response.Cookies = append(jpt.response.Cookies, &carpdriver.Cookie{
				Path:  jpt.response.Path,
				Name:  k,
				Value: v,
			})
		}
	}

	return nil
}

func (jpt *jwtPluginTest) setBody(body *godog.DocString) error {
	if jpt.request.ID != uuid.Nil {
		jpt.request.Body = []byte(body.Content)
	} else {
		jpt.response.Body = []byte(body.Content)
	}

	return nil
}

func (jpt *jwtPluginTest) setStatus(status int) error {
	jpt.response.Status = status
	return nil
}

func (jpt *jwtPluginTest) requestStart() error {
	req, res, err := jpt.plugin.RequestStart(jpt.request)
	if err != nil {
		return helpers.AssertError(assert.NoError, err, err.Error())
	}

	if res != nil {
		jpt.result = res
	} else {
		jpt.result = req
	}

	return nil
}

func (jpt *jwtPluginTest) requestEnd() error {
	res, err := jpt.plugin.RequestEnd(jpt.response)
	if err != nil {
		return helpers.AssertError(assert.NoError, err, err.Error())
	}

	jpt.result = res
	return nil
}

func (jpt *jwtPluginTest) shouldHoldHeaders(r string, headers *godog.Table) error {
	assist := assistdog.NewDefault()
	result, err := assist.ParseMap(headers)
	if err != nil {
		return helpers.AssertError(assert.NoError, err, err.Error())
	}

	actualHeaders := make(map[string]string)

	if r == "request" {
		request := jpt.result.(*carpdriver.Request)
		actualHeaders = request.Headers
	} else {
		response := jpt.result.(*carpdriver.Response)
		actualHeaders = response.Headers
	}

	return helpers.AssertExpectedActual(
		assert.Equal, result, actualHeaders,
		"Expected %v, but there is %v", result, actualHeaders,
	)
}

func (jpt *jwtPluginTest) responseShouldNoExists() error {
	response := jpt.result.(*carpdriver.Response)
	return helpers.AssertActual(
		assert.Empty, response,
		"Expected a empty response, but there is %v", response,
	)
}

func (jpt *jwtPluginTest) responseShouldHoldId(id string) error {
	response := jpt.result.(*carpdriver.Response)
	return helpers.AssertExpectedActual(
		assert.Equal, id, response.ID.String(),
		"Expected %v, but there is %v", id, response.ID.String(),
	)
}

func (jpt *jwtPluginTest) responseShouldHoldPath(path string) error {
	response := jpt.result.(*carpdriver.Response)
	return helpers.AssertExpectedActual(
		assert.Equal, path, response.Path,
		"Expected %v, but there is %v", path, response.Path,
	)
}

func (jpt *jwtPluginTest) responseShouldHoldNoCookies() error {
	response := jpt.result.(*carpdriver.Response)
	return helpers.AssertActual(
		assert.Empty, response.Cookies,
		"Expected no cookies, but there is %v", response.Cookies,
	)
}

func (jpt *jwtPluginTest) responseShouldHoldStatus(status int) error {
	response := jpt.result.(*carpdriver.Response)
	return helpers.AssertExpectedActual(
		assert.Equal, status, response.Status,
		"Expected %v, but there is %v", status, response.Status,
	)
}

func (jpt *jwtPluginTest) responseShouldHoldBody(body *godog.DocString) error {
	response := jpt.result.(*carpdriver.Response)
	return helpers.AssertExpectedActual(
		assert.Equal, []byte(body.Content), response.Body,
		"Expected %v, but there is %v", body.Content, string(response.Body),
	)
}

func (jpt *jwtPluginTest) responseShouldHoldBodyStart(body *godog.DocString) error {
	response := jpt.result.(*carpdriver.Response)
	start := strings.HasPrefix(string(response.Body), body.Content)
	return helpers.AssertExpectedActual(
		assert.Equal, start, true,
		"Expected %v, but there is %v", body.Content, string(response.Body),
	)
}
