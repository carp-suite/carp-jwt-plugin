package helpers

import (
	"fmt"

	"github.com/stretchr/testify/assert"
)

type asserter struct {
	err error
}

func (a *asserter) Errorf(format string, args ...interface{}) {
	a.err = fmt.Errorf(format, args...)
}

type (
	errorAssertion          func(t assert.TestingT, actual error, msgAndArgs ...interface{}) bool
	actualAssertion         func(t assert.TestingT, actual interface{}, msgAndArgs ...interface{}) bool
	expectedActualAssertion func(t assert.TestingT, expected, actual interface{}, msgAndArgs ...interface{}) bool
)

func AssertError(a errorAssertion, actual error, msgAndArgs ...interface{}) error {
	var t asserter
	a(&t, actual, msgAndArgs...)
	return t.err
}

func AssertActual(a actualAssertion, actual interface{}, msgAndArgs ...interface{}) error {
	var t asserter
	a(&t, actual, msgAndArgs...)
	return t.err
}

func AssertExpectedActual(a expectedActualAssertion, expected, actual interface{}, msgAndArgs ...interface{}) error {
	var t asserter
	a(&t, expected, actual, msgAndArgs...)
	return t.err
}
