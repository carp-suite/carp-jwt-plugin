Feature: JWT Plugin Service
  The plugin override request to web servers when the request start
  and add a session cookie to request when the request end

  Scenario: Override plugin request
    Given the next configuration:
		"""
		{
			"cookieName": "JWT",
			"secretKey": "hello"
		}
		"""
    And the request have id e38a9003-bc2d-4ba3-ac5d-d6a8c74de162
    And method get
    And path /
		And headers 
			| Content-Length | 0         |
			| Host           | localhost |
      | Cookie         | JWT=eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.Brp7tDCUj3wlZtF6a15KW0A7wLJbnDLOFky03GY9vSdEYo-RlwFCIqpzFV0hHsH5_A7pA28yrRFPqyTSsumZfQ |
		And body
		"""
			hello world
		"""
    When the request start
    Then the request should hold the following headers:
			| Content-Length | 0         |
			| Host           | localhost |
      | Cookie         | JWT=eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.Brp7tDCUj3wlZtF6a15KW0A7wLJbnDLOFky03GY9vSdEYo-RlwFCIqpzFV0hHsH5_A7pA28yrRFPqyTSsumZfQ |
			| Authorization  | Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.Brp7tDCUj3wlZtF6a15KW0A7wLJbnDLOFky03GY9vSdEYo-RlwFCIqpzFV0hHsH5_A7pA28yrRFPqyTSsumZfQ |
    And request should hold body
		"""
			hello world
		"""

  Scenario: Fail override plugin request because cookie no exists
    Given the next configuration:
		"""
		{
			"cookieName": "JWT",
			"secretKey": "hello"
		}
		"""
    And the request have id e38a9003-bc2d-4ba3-ac5d-d6a8c74de162
    And method get
    And path /
		And headers 
			| Content-Length | 11        |
			| Host           | localhost |
		And body
		"""
		hello world
		"""
    When the request start
    And the response should hold the following headers:
      | Content-Type   | text/plain |
			| Content-Length | 20         |
    And hold status 403
    And response should hold body
    """
    JWT cookie not found
    """

  Scenario: Override plugin response
    Given the next configuration:
		"""
		{
			"cookieName": "JWT",
			"path": "/a",
			"status": 200
		}
		"""
    And the response have id e38a9003-bc2d-4ba3-ac5d-d6a8c74de162
		And headers 
      | Content-Type   | text/plain |
			| Content-Length | 198        |
    And path /app
    And status 200
		And body
    """
    eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.Brp7tDCUj3wlZtF6a15KW0A7wLJbnDLOFky03GY9vSdEYo-RlwFCIqpzFV0hHsH5_A7pA28yrRFPqyTSsumZfQ
    """
    When the request end
    Then the response should hold the following headers:
      | Content-Type   | text/plain |
			| Content-Length | 0          |
      | Set-Cookie     | JWT=eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.Brp7tDCUj3wlZtF6a15KW0A7wLJbnDLOFky03GY9vSdEYo-RlwFCIqpzFV0hHsH5_A7pA28yrRFPqyTSsumZfQ |
    And hold path /app
    And hold status 200
    And response should hold body
    """
    """
