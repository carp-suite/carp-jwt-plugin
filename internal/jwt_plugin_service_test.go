package internal

import (
	"context"
	"os"
	"testing"

	"github.com/cucumber/godog"
	"github.com/cucumber/messages-go/v16"
	fbs "github.com/google/flatbuffers/go"
	"github.com/google/uuid"
	"github.com/rdumont/assistdog"
	"github.com/stretchr/testify/assert"
	"github.com/wapc/wapc-go"
	"github.com/wapc/wapc-go/engines/wazero"
	"gitlab.com/carp-suite/plugins/carp-jwt-plugin/internal/helpers"
	"gitlab.com/carp-suite/plugins/carpdriver/flatbuffers"
)

type jwtPluginServiceTest struct {
	count         int
	ctx           context.Context
	module        wapc.Module
	instance      wapc.Instance
	result        []byte
	reqBuilder    *fbs.Builder
	resBuilder    *fbs.Builder
	headerBuilder *fbs.Builder
	request       struct {
		id      fbs.UOffsetT
		path    fbs.UOffsetT
		method  int16
		headers fbs.UOffsetT
		body    fbs.UOffsetT
	}
	response struct {
		id      fbs.UOffsetT
		path    fbs.UOffsetT
		status  int16
		headers fbs.UOffsetT
		body    fbs.UOffsetT
	}
}

func TestJwtPlugin(t *testing.T) {
	suite := godog.TestSuite{
		ScenarioInitializer: InitializeScenario,
		Options: &godog.Options{
			Format:   "pretty",
			Paths:    []string{"./jwt_plugin_service.feature"},
			TestingT: t,
		},
	}

	if suite.Run() != 0 {
		t.Fatal("non-zero status returned, failed to run plugin tests")
	}
}

func InitializeScenario(sc *godog.ScenarioContext) {
	jpst := &jwtPluginServiceTest{}

	sc.Before(func(ctx context.Context, sc *godog.Scenario) (context.Context, error) {
		err := jpst.setup(sc)
		return ctx, err
	})

	sc.Step(`^the next configuration:$`, jpst.configure)
	sc.Step(`^the (\w+) have id (.+)$`, jpst.setId)
	sc.Step(`^method (\w+)$`, jpst.setMethod)
	sc.Step(`^path (.+)$`, jpst.setPath)
	sc.Step(`^status (\d+)$`, jpst.setStatus)
	sc.Step(`^headers$`, jpst.setHeaders)
	sc.Step(`^body$`, jpst.setBody)
	sc.Step(`^the request start$`, jpst.requestStart)
	sc.Step(`^the request end$`, jpst.requestEnd)
	sc.Step(`^the (\w+) should hold the following headers:$`, jpst.shouldHoldHeaders)
	sc.Step(`^hold status (\d+)$`, jpst.shouldHoldStatus)
	sc.Step(`^hold path (.+)$`, jpst.shouldHoldPath)
	sc.Step(`^(\w+) should hold body$`, jpst.shouldHoldBody)

	sc.AfterScenario(func(sc *messages.Pickle, err error) {
		jpst.close()
	})
}

func (jpst *jwtPluginServiceTest) mockHost(ctx context.Context, binding, namespace, operation string, payload []byte) ([]byte, error) {
	return []byte("default"), nil
}

func (jpst *jwtPluginServiceTest) setup(*godog.Scenario) error {
	var err error
	guest, err := os.ReadFile("../plugin.wasm")
	if err != nil {
		return helpers.AssertError(assert.NoError, err, err.Error())
	}

	jpst.ctx = context.Background()
	engine := wazero.Engine()

	jpst.module, err = engine.New(jpst.ctx, jpst.mockHost, guest, &wapc.ModuleConfig{
		Logger: wapc.PrintlnLogger,
		Stdout: os.Stdout,
		Stderr: os.Stderr,
	})
	if err != nil {
		return helpers.AssertError(assert.NoError, err, err.Error())
	}

	jpst.instance, err = jpst.module.Instantiate(jpst.ctx)
	if err != nil {
		return helpers.AssertError(assert.NoError, err, err.Error())
	}

	return nil
}

func (jpst *jwtPluginServiceTest) close() error {
	jpst.module.Close(jpst.ctx)
	jpst.instance.Close(jpst.ctx)
	return nil
}

func (jpst *jwtPluginServiceTest) configure(data *godog.DocString) error {
	_, err := jpst.instance.Invoke(jpst.ctx, "configure", []byte(data.Content))
	if err != nil {
		return helpers.AssertError(assert.NoError, err, err.Error())
	}

	return nil
}

func (jpst *jwtPluginServiceTest) setId(r string, id string) error {
	uid, err := uuid.Parse(id)
	if err != nil {
		return helpers.AssertError(assert.NoError, err, err.Error())
	}

	if r == "request" {
		jpst.reqBuilder = new(fbs.Builder)
		jpst.request.id = jpst.reqBuilder.CreateByteVector(uid[:])

	} else {
		jpst.resBuilder = new(fbs.Builder)
		jpst.response.id = jpst.resBuilder.CreateByteVector(uid[:])
	}

	return nil
}

func (jpst *jwtPluginServiceTest) setPath(path string) error {
	if jpst.reqBuilder != nil {
		jpst.request.path = jpst.reqBuilder.CreateByteString([]byte(path))
	} else {
		jpst.response.path = jpst.resBuilder.CreateByteString([]byte(path))
	}

	return nil
}

func (jpst *jwtPluginServiceTest) setMethod(method string) error {
	methods := make(map[string]int16)
	methods["get"] = 1
	methods["post"] = 2
	methods["patch"] = 3
	methods["put"] = 4
	methods["delete"] = 5
	methods["options"] = 6

	jpst.request.method = methods[method]
	return nil
}

func (jpst *jwtPluginServiceTest) setStatus(status int) error {
	jpst.response.status = int16(status)
	return nil
}

func (jpst *jwtPluginServiceTest) setHeaders(payload *godog.Table) error {
	assist := assistdog.NewDefault()
	result, err := assist.ParseMap(payload)
	if err != nil {
		return helpers.AssertError(assert.NoError, err, err.Error())
	}

	if jpst.reqBuilder != nil {
		headers := make([]fbs.UOffsetT, len(result))
		for k, v := range result {
			key := jpst.reqBuilder.CreateByteString([]byte(k))
			value := jpst.reqBuilder.CreateByteString([]byte(v))
			flatbuffers.HeaderStart(jpst.reqBuilder)
			flatbuffers.HeaderAddKey(jpst.reqBuilder, key)
			flatbuffers.HeaderAddValue(jpst.reqBuilder, value)
			headers = append(headers, flatbuffers.HeaderEnd(jpst.reqBuilder))
		}

		flatbuffers.RequestStartHeadersVector(jpst.reqBuilder, len(headers))
		for _, header := range headers {
			jpst.reqBuilder.PrependUOffsetT(header)
		}
		jpst.request.headers = jpst.reqBuilder.EndVector(len(headers))

	} else {
		headers := make([]fbs.UOffsetT, len(result))
		for k, v := range result {
			key := jpst.resBuilder.CreateByteString([]byte(k))
			value := jpst.resBuilder.CreateByteString([]byte(v))
			flatbuffers.HeaderStart(jpst.resBuilder)
			flatbuffers.HeaderAddKey(jpst.resBuilder, key)
			flatbuffers.HeaderAddValue(jpst.resBuilder, value)
			headers = append(headers, flatbuffers.HeaderEnd(jpst.resBuilder))
		}

		flatbuffers.ResponseStartHeadersVector(jpst.resBuilder, len(headers))
		for _, header := range headers {
			jpst.resBuilder.PrependUOffsetT(header)
		}
		jpst.response.headers = jpst.resBuilder.EndVector(len(headers))
	}

	return nil
}

func (jpst *jwtPluginServiceTest) setBody(body *godog.DocString) error {
	// jpst.body = body.Content
	if jpst.reqBuilder != nil {
		jpst.request.body = jpst.reqBuilder.CreateByteString([]byte(body.Content))
	} else {
		jpst.response.body = jpst.resBuilder.CreateByteString([]byte(body.Content))
	}

	return nil
}

func (jpst *jwtPluginServiceTest) requestStart() error {
	flatbuffers.RequestStart(jpst.reqBuilder)
	flatbuffers.RequestAddId(jpst.reqBuilder, jpst.request.id)
	flatbuffers.RequestAddPath(jpst.reqBuilder, jpst.request.path)
	flatbuffers.RequestAddMethod(jpst.reqBuilder, jpst.request.method)
	flatbuffers.RequestAddBody(jpst.reqBuilder, jpst.request.body)
	flatbuffers.RequestAddHeaders(jpst.reqBuilder, jpst.request.headers)
	reqRoot := flatbuffers.RequestEnd(jpst.reqBuilder)
	jpst.reqBuilder.Finish(reqRoot)

	var err error
	jpst.result, err = jpst.instance.Invoke(jpst.ctx, "request_start", jpst.reqBuilder.FinishedBytes())
	if err != nil {
		return helpers.AssertError(assert.NoError, err, err.Error())
	}

	return nil
}

func (jpst *jwtPluginServiceTest) requestEnd() error {
	flatbuffers.ResponseStart(jpst.resBuilder)
	flatbuffers.ResponseAddId(jpst.resBuilder, jpst.response.id)
	flatbuffers.ResponseAddPath(jpst.resBuilder, jpst.response.path)
	flatbuffers.ResponseAddStatus(jpst.resBuilder, jpst.response.status)
	flatbuffers.ResponseAddBody(jpst.resBuilder, jpst.response.body)
	flatbuffers.ResponseAddHeaders(jpst.resBuilder, jpst.response.headers)
	resRoot := flatbuffers.ResponseEnd(jpst.resBuilder)
	jpst.resBuilder.Finish(resRoot)

	var err error
	jpst.result, err = jpst.instance.Invoke(jpst.ctx, "request_end", jpst.resBuilder.FinishedBytes())
	if err != nil {
		return helpers.AssertError(assert.NoError, err, err.Error())
	}

	return nil
}

func (jpst *jwtPluginServiceTest) shouldHoldHeaders(r string, headers *godog.Table) error {
	assist := assistdog.NewDefault()
	result, err := assist.ParseMap(headers)
	if err != nil {
		return helpers.AssertError(assert.NoError, err, err.Error())
	}

	actualHeaders := make(map[string]string)

	if r == "request" {
		request := flatbuffers.GetRootAsRequest(jpst.result, 0)

		h := new(flatbuffers.Header)
		for i := 0; i < len(result); i++ {
			exists := request.Headers(h, i)
			if !exists {
				continue
			}
			actualHeaders[string(h.Key())] = string(h.Value())
		}

	} else {
		response := flatbuffers.GetRootAsResponse(jpst.result, 0)

		h := new(flatbuffers.Header)
		for i := 0; i < len(result); i++ {
			exists := response.Headers(h, i)
			if !exists {
				continue
			}
			actualHeaders[string(h.Key())] = string(h.Value())
		}
	}

	return helpers.AssertExpectedActual(
		assert.Equal, result, actualHeaders,
		"Expected %v, but there is %v", result, actualHeaders,
	)
}

func (jpst *jwtPluginServiceTest) shouldHoldStatus(status int) error {
	response := flatbuffers.GetRootAsResponse(jpst.result, 0)
	actualStatus := int(response.Status())
	return helpers.AssertExpectedActual(
		assert.Equal, status, actualStatus,
		"Expected %d, but there is %d", status, actualStatus,
	)
}

func (jpst *jwtPluginServiceTest) shouldHoldPath(path string) error {
	response := flatbuffers.GetRootAsResponse(jpst.result, 0)
	actualPath := response.Path()
	return helpers.AssertExpectedActual(
		assert.Equal, []byte(path), actualPath,
		"Expected %s, but there is %s", path, string(actualPath),
	)
}

func (jpst *jwtPluginServiceTest) shouldHoldBody(r string, body *godog.DocString) error {
	var actualBody []byte
	if r == "request" {
		request := flatbuffers.GetRootAsRequest(jpst.result, 0)
		actualBody = request.BodyBytes()
	} else {
		response := flatbuffers.GetRootAsResponse(jpst.result, 0)
		actualBody = response.BodyBytes()
	}

	return helpers.AssertExpectedActual(
		assert.Equal, []byte(body.Content), actualBody,
		"Expected %s, but there is %s", body.Content, string(actualBody),
	)
}
